//import * posts as data from "./db/posts.json";

const express = require('express');
const bodyParser = require('body-parser');
const users = [];
let posts = require('./views/db/posts.json');

const app = express();

const router = require('./router');
app.use(router);

app.use(express.json());

/* 
GET all data
endpoint: http://localhost:3000/api/v1/posts for get all data, method -> GET
*/
app.get('/api/v1/posts', (req, res) => {
  res.status(200).json(posts);
});

app.get('/api/v1/posts/:id', (req, res) => {
  const post = posts.find((i) => i.id === +req.params.id);
  res.status(200).json(post);
});

app.use(bodyParser.urlencoded({ extended: true }));

app.use(express.static('public'));
app.use(express.static('views'));

app.set('view engine', 'ejs');

app.get('/greet', (req, res) => {
  const name = req.query.name || 'Afrizal';
  res.render('greet', {
    name,
  });
});

app.get('/', function (req, res) {
  res.render('body');
});
app.get('/body', function (req, res) {
  res.render('body');
});
app.get('/about', function (req, res) {
  res.render('about');
});
app.get('/rps', function (req, res) {
  res.render('rps');
});
app.get('/login', function (req, res) {
  res.render('login');
});

app.post('/login', (req, res) => {
  const { name, password } = req.body;

  users.push({ name, password });
  res.redirect('/greet');
});
app.listen(3000, function () {
  console.log('Server running on port 3000');
});
